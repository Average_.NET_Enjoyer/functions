const getSum = (str1, str2) => {
  if (typeof(str1) !== 'string' || typeof(str2) !== 'string') {
    return false;
  }

  if (str1.length === 0) {
    for (let i = 0; i < str2.length; i++)
      str1 += '0';
  }

  if (isNaN(parseInt(str1)) || isNaN(parseInt(str2)))
    return false;

  let resultedString = '';

  for (let i = 0; i < str2.length; i++) {
    resultedString += `${parseInt(str1[i]) + parseInt(str2[i])}`;
  }

  return resultedString;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let resultedString = 'Post:0,comments:0';
  let postsCount = 0, commentsCount = 0;

  for (const post of listOfPosts) {
    if (post.author === authorName)
      postsCount++;
  }

  for (const post of listOfPosts) {

    if (!post.comments) continue;

    for (const comment of post.comments) {
      if (comment.author === authorName)
        commentsCount++;
    }
  }

  resultedString = `Post:${postsCount},comments:${commentsCount}`;

  return resultedString;
};

const tickets=(people)=> {
  let max = Number.MIN_VALUE;
  let sum = 0;

  for (const person of people)
    if (max < person)
      max = person;
  
  for (const person of people) {
    if (person === max)
      continue;
    sum += person;
  }

  if (sum >= max)
    return 'YES';
  else
    return 'NO';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
